# reporting-tools

These scripts should help getting stats and information about what is going in in KDE PIM and scans through the git history.

It requires at the moment all repos (`REPO_BASE`)  in the same folder and that the checkout is up-to-date.

## `authors-activity.py`

Shows all activity of authots around KDE PIM repos.

Needs [GitPython](https://github.com/gitpython-developers/GitPython) to be installed on your system. (It is named python3-git on Debian/Ubuntu/Neon...).

It supports `--help` to show you more options.

Sample run:
```
  cd REPO_BASE
  PATHTO/authors-activity.py "Sandro Knauß"
```
