#! /usr/bin/env python3
#
# SPDX-FileCopyrightText: 2022 Sandro Knauß <sknauss@kde.org>
# SPDX-License-Identifier: GPL-2.0-or-later

"""
Script to show activity of authors in KDE PIM around the last months.

Typically you run this script in the base dir of all KDE PIM repos checkout.
You need to update the repos first:

$ authors-activity.py "Sandro Knauß"

it support --help to show you more options to treat
"""

import argparse
import calendar
import datetime
import logging
import pathlib

import git

KDEPIM_MODULES_PATH = pathlib.Path(__file__).with_name("kdepim.modules").absolute()


class Message:
    def __init__(self, module: str, commit: git.Commit):
        self.module = module
        self.commit = commit

    @property
    def author(self) -> str:
        return self.commit.author

    @property
    def headline(self) -> str:
        headline, *msg = self.commit.message.splitlines()
        return headline

    @property
    def shorthash(self) -> str:
        return self.commit.hexsha[:9]

    def __str__(self):
        return f"{self.module}({self.shorthash}) {self.author} -  {self.headline}"


def two_month_before(date: datetime.date):
    m = date.month - 1
    y = date.year
    if m == 0:
        m = 12
        y -= 1
    _, days = calendar.monthrange(y, m)
    return date - datetime.timedelta(days=date.day + days - 1)


def date_parser(s: str) -> datetime.date:
    return datetime.datetime.strptime(s, "%Y-%m-%d").date()


def main():
    logging.basicConfig(
        level=logging.INFO,
        format="%(levelname)s: %(message)s",
    )

    now = datetime.date.today()

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--mindate",
        help="The minimal date until git commits are scanned.",
        default=two_month_before(now),
        type=date_parser,
    )
    parser.add_argument(
        "--maxdate",
        help="The maximun date until git commits are scanned.",
        default=now,
        type=date_parser,
    )
    parser.add_argument("authors", help="Authors to search for", nargs="*")

    args = parser.parse_args()
    args.authors = set(i.lower() for i in args.authors)

    kde_modules = {i: None for i in KDEPIM_MODULES_PATH.read_text().splitlines()}

    for r in pathlib.Path(".").glob("*"):
        if not r.name in kde_modules:
            continue
        repo = git.Repo(r)
        kde_modules[r.name] = repo

    messages = []
    # iterate through all modules
    for module, m in kde_modules.items():
        if m is None:
            logging.warning(f"{module}: Please checkout to current working directory.")
            continue
        for commit in m.iter_commits():
            authored_date = commit.authored_datetime.date()
            if authored_date > args.maxdate:
                continue
            if authored_date < args.mindate:
                break

            for n in args.authors:
                if n in commit.author.name.lower():
                    messages.append(Message(module, commit))

    for msg in messages:
        print(msg)


if __name__ == "__main__":
    main()
