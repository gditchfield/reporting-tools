#!/bin/sh
# SPDX-FileCopyrightText: 2022 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: GPL-2.0-or-later

moduleset=$1
shift
if [ -z "$moduleset" ]; then
    echo "Usage: $0 <moduleset>"
    exit 1
fi

starttime=$1
if [ -z "$starttime" ]; then
    starttime="2month"
fi

thisdir=`dirname $0`
thisdir=`realpath $thisdir`
basedir=/k/kde5/src
gitcmd="git --no-pager log --no-merges --since ${starttime} --pretty=%an_%ae"
buffer=$thisdir/commit-stat.tmp
rm -f $buffer
touch $buffer

for mod in `cat $thisdir/$moduleset.modules`; do
    pushd $basedir/$mod > /dev/null
    $gitcmd | grep -v "l10n daemon script" >> $buffer
    popd > /dev/null
done

cat $buffer | sort | uniq -c | sort
echo -n "Committer: "
cat $buffer | sort | uniq | wc -l
echo -n "Total: "
cat $buffer | wc -l
rm $buffer

