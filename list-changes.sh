#!/bin/sh
# SPDX-FileCopyrightText: 2022 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: GPL-2.0-or-later

moduleset=$1
shift
if [ -z "$moduleset" ]; then
    echo "Usage: $0 <moduleset>"
    exit 1
fi

starttime=$1
if [ -z "$starttime" ]; then
    starttime="2month"
fi

thisdir=`dirname $0`
basedir=/k/kde5/src
gitcmd="git --no-pager log --no-merges --since ${starttime}"
#gitcmd="git --no-pager log --no-merges v21.12.1...release/21.12"
#gitcmd="git --no-pager log --no-merges v5.88.0...v5.89.0"
parsecmd="$basedir/release-tools/parse_changelogs.pl"

for mod in `cat $thisdir/$moduleset.modules`; do
    pushd $basedir/$mod > /dev/null
    if [ -f README.md ]; then
        ( head -n1 README.md ; $gitcmd ) | $parsecmd
    else
        ( echo " $mod" ; $gitcmd ) | $parsecmd
    fi
    echo ""
    popd > /dev/null
done

